<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>

<body>
    <h1>{{ $project->title }}</h1>
    <h2>{{ $project->description }}</h2>

    @foreach ($project->tasks as $task)
        <li>{{ $task->description }}</li>
    @endforeach
</body>

</html>