<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>

<body>
    <h1>Create New Project</h1>

    <form method="POST" action="/projects">
        {{ csrf_field() }}
        
        <div>
            <input type="text" name="title" placeholder="Title of project">
        </div>

        <div>
            <textarea name="description" placeholder="Describe your cute little project"></textarea>
        </div>

        <div>
            <button type="submit">Create project</button>
        </div>
    </form>
</body>

</html>